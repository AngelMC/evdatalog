#include "SIM900.h"
#include <SoftwareSerial.h>
#include "gps.h"



//To change pins for Software Serial, use the two lines in GSM.cpp.

//GSM Shield for Arduino
//www.open-electronics.org
//this code is based on the example of Arduino Labs.


GPSGSM gps;

char lon[15];
char lat[15];
char alt[15];
char time[20];
char vel[15];


char stat;

int i=0;
boolean started=false;

void gps_read();

void setup() 
{
  //Serial connection.
  Serial.begin(9600);
  Serial.println("GSM Shield testing.");
  //Start configuration of shield with baudrate.
  //For http uses is raccomanded to use 4800 or slower.
  if (gsm.begin(9600)){
    Serial.println("\nstatus=READY");
    gsm.forceON();	//To ensure that SIM908 is not only in charge mode
    started=true;  
  }
  else Serial.println("\nstatus=IDLE");
  
  if(started){
    //GPS attach
    if (gps.attachGPS())
      Serial.println("status=GPSREADY");
    else Serial.println("status=ERROR");
	
    delay(5000);	//Time for fixing
    gps_read();
  }
  
  
}

void loop() 
{

gps_read();
  
}


void gps_read(){
 
  stat=gps.getStat();
	if(stat==1)
		Serial.println("NOT FIXED");
	else if(stat==0)
		Serial.println("GPS OFF");
	else if(stat==2)
		Serial.println("2D FIXED");
	else if(stat==3)
		Serial.println("3D FIXED");
  gps.getPar(lon,lat,alt,time,vel);
	Serial.println(lon);
	Serial.println(lat);
	Serial.println(alt);
        Serial.println("\n");
	//Serial.println(time);
	//Serial.println(vel);
  delay(1000);

}

