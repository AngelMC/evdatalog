#ifndef SIMCOM900_H
#define SIMCOM900_H
#include <SoftwareSerial.h>
#include "HWSerial.h"
#include "GSM.h"

class SIMCOM900 : public virtual GSM
{


  public:
    SIMCOM900();
    ~SIMCOM900();

	char forceON();
    virtual int read(char* result, int resultlength);
	virtual uint8_t read();
    void SimpleRead();
    void WhileSimpleRead();
    void SimpleWrite(char *comm);
    void SimpleWrite(char const *comm);
    void SimpleWrite(int comm);
	void SimpleWrite(const __FlashStringHelper *pgmstr);
    void SimpleWriteln(char *comm);
    void SimpleWriteln(char const *comm);
	void SimpleWriteln(const __FlashStringHelper *pgmstr);
    void SimpleWriteln(int comm);
};

extern SIMCOM900 gsm;

#endif

