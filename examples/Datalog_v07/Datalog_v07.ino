/*
 Revision v0.6:
        - Se ha implementado por software una solucion para eliminar los rebotes del pulsador, descartando el filtro antirebotes mediante hardware usado.
        - Se comprueba el estado de la tarjeta SD cada vez que se crea un nuevo archivo ,test_SD(),
 Revisión v0.5:
 	-Se han cambiado los cuatro led indicadores por un led de tipo rgb. El código de colores implementado para cada fallo es:
 Rojo intermitente: La tarjeta microSD no está conectada o la conexión es incorrecta
 Azul intermitente: No se puede encontrar una particion de tipo FAT16/FAT32 en la SD
 Rojo fijo: Error en la escritura de archivos
 Verde fijo: funcionamiento correcto
 
 Al accionar el pulsador de la interrupción se apaga momentaneamente el LED. Interrupción correctamente ejecutada.
 Cuando se crear un nuevo archivo, al lanzar la interrupción, se ejecuta una secuencia intermitente con el led en color azul. 
 
 	-Se ha actualizado la función testSD con la inclusión de un blucle que actualice el estado de la SD automaticamente, sin necesidad de hacer reset. 
 	-Se ha incluido la función led_rgb para controlar el led de indicadores.
 
 Revisión v.02:
 - Esta version incluye escritura de datos de los sensores de luz y temperatura asi como de los prevenientes del cycle del VE
 
 Parte de  este código está basado en el trabajo de Tom Igoe. Dominio Público
 */

/* 
 Notas:	
 
 **Conexionado para hacer uso del protocolo de comunicación SPI en arduino (conexión con microSD)
 
 * MOSI - pin 11
 * MISO - pin 12
 * CLK - pin 13
 * CS - pin 4
 
 **IMPORTANTE
 Es necesario desconectar el bus de recepción de datos serial (PIN 1 DIGITAL) cuando se actualiza el programa en arduino.
 
 
 
 	 
 */

#include <SD.h>
#include <SPI.h>
Sd2Card card;
SdVolume volume;
SdFile root;
File Archivo;

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int cS = 10;

//Prueba led rgb como indicador 

const int pin_rg_red=5;
const int pin_rg_green=6;

int rg_red;
int rg_green;

volatile boolean new_file=LOW;
volatile long last_Int=0;

String file_name;
byte datos[30];
byte char_final=B1101;



boolean test_SD();
void write_sd(boolean, byte);
void sensar(boolean);
void led_rg(int,int);

void setup()
{
  Serial.begin(9600);

  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(10, OUTPUT);

  //Led rgb
  pinMode(pin_rg_red,OUTPUT);
  pinMode(pin_rg_green,OUTPUT);

  //Interrupcion
  attachInterrupt(0,interrupcion,RISING);  

  if(!test_SD())
    return;
  // see if the card is present, can be initialized and then we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
}


void loop(){


  //Eliminar el bucle anterior de tal forma que comience a sensar cuando se produzca la interrupción asociada a la entrada de datos por Serie.
  sensar(new_file);


}
/*-----------------------------sensar dator----------------*/

void sensar(boolean state){
  delay(1000);
  Serial.println("Sensando...");
  datos[1]=1;
  write_sd(state, datos);
  

}

/*---------------------------------------TEST SD CARD--------------------------------*/
boolean test_SD(){

  boolean test=false;
  Serial.println("Inicializando tarjeta SD...");
  SD.begin(cS);

  while(!test){

    if(!card.init(SPI_HALF_SPEED, cS)){
      led_rg(255,0);
      delay(100);
      led_rg(0,0);

      Serial.println("  Conexion incorrecta con la tarjeta");
      test=false;  
    }

    else if ( !volume.init(card)) {
      led_rg(155,155);
      delay(100);
      led_rg(0,0);
      delay(200);
      Serial.println("  No se puede encontrar una particion FAT16/FAT32");
      test=false; 
    } 

    else {
      led_rg(0,255);
      Serial.println("  Conexion correcta SD");
      test=true;
    } 
  }


  // listar todos los archivos presentes en la tarjeta
  /*
     Serial.println("\nArchivos encontrados (Nombre, Fecha, Tamaño en Bytes): ");
   root.openRoot(volume);
   root.ls(LS_R | LS_DATE | LS_SIZE);
   */


  return test;


}

//-------------Write SD---------------//
void write_sd(boolean std, byte datos[]){
  int j=0;
  if(!std && test_SD()){
    for(int i=1;;i++){

      file_name = "data";
      file_name.concat(i);
      file_name.concat(".log");
      char filename[file_name.length()+1];

      file_name.toCharArray(filename, sizeof(filename));
      if(!SD.exists(filename))
      {
        Serial.println("Creando nuevo archivo...");
        Archivo = SD.open(filename,FILE_WRITE);
        Archivo.println("  Ah     V    A     S    D3");
        Archivo.close(); 
        new_file=HIGH;

        //Indica mediate led que se ha creado el archivo correctamente.
        led_rg(155,155);
        delay(50);
        led_rg(0,0);
        delay(100);
        led_rg(155,155);
        delay(250);
        led_rg(0,255);
        break;
      }

    }
  }
  else{
    char filename[file_name.length()+1];
    file_name.toCharArray(filename, sizeof(filename));
    Archivo=SD.open(filename,FILE_WRITE);


    if (Archivo) {
      
      while(datos[j]!=char_final){
        if(datos[j]==32)
          Archivo.print(",");
        else
          Archivo.print(char(datos[j]));
        j++;
      } 
      //Archivo.println();
      Archivo.close();
      Serial.println("Datos guardados");
      led_rg(255,255);
      delay(50);
      led_rg(0,0);
      delay(50);
      led_rg(255,0);
    } 
    else {
      Serial.println("Error al leer el archivo");
      led_rg(255,0);
    }

  }
}
//---------------------------- Interrupcion--------------------------------------//
void interrupcion(){
  if((millis()-last_Int)>500){
    new_file=LOW;
    led_rg(0,0);
    delay(3000);
    led_rg(0,255);
    last_Int=millis();
  }

}


// -----------------------------LED RGB-------------------------------------//

void led_rg(int rg_red,int rg_green){

  analogWrite(pin_rg_red,rg_red);
  analogWrite(pin_rg_green,rg_green);

}


