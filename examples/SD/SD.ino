/*
  SD card datalogger
 
 This example shows how to log data from three analog sensors 
 to an SD card using the SD library.
 	
 The circuit:
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 
 created  24 Nov 2010
 updated 2 Dec 2010
 by Tom Igoe
 
 This example code is in the public domain.
 	 
 */

#include <SD.h>
Sd2Card card;
SdVolume volume;
SdFile root;
File Archivo1;

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int cS = 4;
int led_ok=8;
int led_fall=9;

boolean test_SD();
void setup()
{
  Serial.begin(9600);
 
  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(10, OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(8,OUTPUT);
  // see if the card is present, can be initialized and then we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
 
if(!test_SD())
  return;

  
  for(int i=1;;i++)
{
  String temp = "data";
  temp.concat(i);
  temp.concat(".csv");
   char filename[temp.length()+1];
   temp.toCharArray(filename, sizeof(filename));
   if(!SD.exists(filename))
   {
     Archivo1 = SD.open(filename,FILE_WRITE);
      break;
   }
}
 /*
  if (Archivo1) {
    Archivo1.println("dghgjhgggggggggggggg");
    Archivo1.close();
  } else {
  
    Serial.println("Error al crear el archivo inicial");
  }
  
  SD.mkdir("angel");
  
  */
  attachInterrupt(2,sensar,RISING);
  pinMode(5,OUTPUT);
}

volatile int state=LOW;
void loop()
{

digitalWrite(5,state);
  
     
 
  
}
/*-----------------------------sensar dator----------------*/

void sensar(){
state=!state;



}







/*---------------------------------------TEST SD CARD--------------------------------*/
boolean test_SD(){
  
  boolean test;
   Serial.print("Initializing SD card...");
   SD.begin(cS);
   
   if (!card.init(SPI_HALF_SPEED, cS) || !volume.init(card)) {
    digitalWrite(led_fall,HIGH);
    test=false;
    //return;  
  } 
  else {
   digitalWrite(led_ok,HIGH);
   test=true;
  }
    
  Serial.println("\nFiles found on the card (name, date and size in bytes): ");
  root.openRoot(volume);
  
  // list all files in the card with date and size
  root.ls(LS_R | LS_DATE | LS_SIZE);
  
  
  
return test;
  

}



